import os
from contextlib import asynccontextmanager
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from api.routes import spaces
from api.core.config import settings
from api.core.database import init_db

# uncoment the following method and app declaration
# if you want to create the database tables from sqlalchemy
# instead of using alembic
# @asynccontextmanager
# async def lifespan(app: FastAPI):
#     init_db()
#     yield

# app = FastAPI(title=settings.PROJECT_NAME, lifespan=lifespan)

#testing if database is connected

app = FastAPI(title=settings.PROJECT_NAME)  # comment this line if you are not using alembic

# binds path resolution to the main.py file directory, instead of the user working directory
project_dir = os.path.dirname(os.path.abspath(__file__))

app.mount("/api/static", StaticFiles(directory=os.path.join(project_dir,"static")), name="static")

templates = Jinja2Templates(directory=os.path.join(project_dir,"templates"))

app.include_router(router=spaces.router, prefix='/spaces')


@app.get("/", response_class=HTMLResponse)
def index(request: Request):
    return templates.TemplateResponse(
        request=request,
        name="index.html",
        context={"title": settings.PROJECT_NAME}
        )


if __name__ == "__main__":
    import uvicorn
    # not suitable for production
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
