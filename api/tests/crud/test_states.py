import pytest
from sqlmodel import Session
from api.crud.states import StateCRUD
from api.deps import get_ibge_endpoint

@pytest.mark.asyncio
async def test_get_states(session: Session):
    stateCrud = StateCRUD(session,get_ibge_endpoint())
    states = await stateCrud.get_states()
    assert len(states) > 0
