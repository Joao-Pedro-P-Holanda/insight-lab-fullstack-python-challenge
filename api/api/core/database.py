from typing import Generator
from .config import settings
from sqlmodel import create_engine, Session, SQLModel
import api.models

engine = create_engine(settings.SQLALCHEMY_DATABASE_URI, echo=True)


def get_session() -> Generator[Session, None, None]:
    with Session(engine) as session:
        yield session


def init_db() -> None:
    SQLModel.metadata.create_all(engine)
