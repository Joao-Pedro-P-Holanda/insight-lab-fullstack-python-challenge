
from pydantic_core import Url


def append_paths(base_url: Url, *args: str) -> str:
    """
    Appends the given strings the base url in the same order
    they are given. Ignores slashes on the args strings.

    :param base_url (str): The base url to append the paths to.
    :param args (str): The paths to append to the base url.

    ### Example:
    ```python
    append_paths("https://servicodados.ibge.gov.br/api/v1/", "localidades", "estados")
    # same as
    append_paths("https://servicodados.ibge.gov.br/api/v1/", "/localidades/","/estados/")
    ```
    """
    if (base_url.scheme is None or base_url.host is None):
        raise ValueError("The base url must have a scheme and a host")

    return str(base_url)+"/".join([s.strip("/") for s in args])
