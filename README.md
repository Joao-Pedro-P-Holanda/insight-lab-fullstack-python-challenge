# Project for the Python Fullstack Developer Challenge

## Description

This workspace contains the implementation of a RESTful API made with FastAPI for the Python Fullstack Developer challenge proposed by Insight Lab for the "Cultura, inovação e inclusão social no Ceará" project.

The objective of this project is to provide a interface to Brazil's cultural data available on the Mapas.cultura.gov.br project, you can visit the project at [this link](https://mapas.cultura.gov.br).

With this API you can fetch for more detailed information that is available through the website, such as: the number of events a agent participated/organized, the space that hosted the larger number of events in a given interval and etc. Projects and Notices were not added to this project.

Addition: due to time constraints only the endpoints for spaces information were added.

Full documentation of all endpoints can be acessed at the [docs](http://127.0.0.1:8000/docs) path.

## Executing

To execute this project locally you need to either have docker with docker-compose installed, or execute every service (Postgres/SQLite, Python, and Node) manually on your machine. I recommend executing with docker-compose.

### With Docker

Add the environment variables at OS-level, or, preferably, in a .env file in the root of this workspace (same folder as compose.yml). The required variables are POSTGRES_USER and POSTGRES_PASSWORD, but you can also specify the POSTGRES_PORT, POSTGRES_HOST and POSTGRES_DB depending on which database you want to connect.

Then, simply run:

```sh
docker compose up
```

and you should have a postgres database running on localhost:5432, the python api running at localhost:8000 and a development server of a React client app running on localhost:5173.

### Without Docker

If you are not using docker you need to define the prior environment variables in a .env file in the same folder as the alembic.ini file _api/api_ (or define at OS-level).

#### Starting the API and Database

To upgrade the database to the latest revision, you need to run the command `alembic upgrade head` as the same user defined in the POSTGRES_USER environment variable.

All dependencies are specified in the requirements.txt file and also in pyproject.toml, you can install all dependencies with pip, or use poetry to automatically manage a virtual environment for you.

- Using Poetry

```sh
cd ./api # api at project level
poetry install

poetry shell

cd ./api # api at package level
uvicorn main:app --host 0.0.0.0
```

- Using pip or another dependency manager (e.g. pipenv)

```sh
# in the workspace root folder
cd ./api
pip install -r requirements.txt # or the equivalent command in pipenv
cd ./api
uvicorn main:app --host 0.0.0.0
```
