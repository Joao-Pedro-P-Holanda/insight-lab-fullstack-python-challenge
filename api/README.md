# Cultural Maps Analytics API

This folder defines the RESTful API created with FastAPI, SQLAlchemy and HTTPX.

Migrations are managed using Alembic, models are defined using SQLModel.
