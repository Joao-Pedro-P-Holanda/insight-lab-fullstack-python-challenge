from datetime import datetime


def str_to_datetime(date: str) -> datetime:
    """Convert a date in the format mm-dd-yyyy to a datetime object"""
    return datetime.strptime(date, "%Y-%m-%d")


def str_to_state_acronym_list(states_as_string: str) -> list[str]:
    """Convert a string of states separated by commas to a list of state acronyms"""
    return states_as_string.split(",")
