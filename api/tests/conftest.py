from typing import Generator
from fastapi.testclient import TestClient
import pytest
from sqlmodel import Session, create_engine, SQLModel
from api.deps import get_session
from api.main import app


@pytest.fixture(scope="session")
def client(session) -> Generator[TestClient, None, None]:
    app.dependency_overrides[get_session] = lambda: session
    client = TestClient(app)
    yield client


@pytest.fixture(scope="session")
def session():
    engine = create_engine('sqlite:///:memory:')

    with Session(engine) as session:
        SQLModel.metadata.create_all(engine)
        yield session
