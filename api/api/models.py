"""
This module include models for report and states data that are
stored in the database, and models for APIs responses (including third-party),
defined with pydantic's BaseModel.
"""
from datetime import datetime
from pydantic import BaseModel
from sqlalchemy import JSON, Column
from sqlmodel import Relationship, SQLModel, Field
from typing import List, Optional


# Defining the models for the database tables
class SpacesReportState(SQLModel, table=True):
    space_report_id: int | None = Field(primary_key=True, foreign_key="spacesreport.id", default=None)
    state_id: int | None = Field(primary_key=True, foreign_key="state.id", default=None)


class State(SQLModel, table=True):
    id: int = Field(primary_key=True)
    name: str = Field(unique=True)
    acronym: str = Field(max_length=2, unique=True)
    region: str
    spaces_report: List["SpacesReport"] = Relationship(back_populates="states", link_model=SpacesReportState)


# BaseReport is a base class for the reports
# states is duplicated in both reports, because non-table models can't have relationships
# and BaseReport shouldn't be a table model
# https://sqlmodel.tiangolo.com/tutorial/fastapi/multiple-models/#only-inherit-from-data-models
class BaseReport(SQLModel):
    # SQLModel creates an autoincrementing id field when default=None is used
    id: int | None = Field(default=None, primary_key=True)
    start_date: datetime
    end_date: datetime


class SpacesReport(BaseReport, table=True):
    total_spaces_count: int
    total_events_count: int
    average_events_count: float
    most_active_state: str
    most_active_space: str
    events_per_state: Optional[dict[str, int]] = Field(default_factory=dict, sa_column=Column(JSON))
    states: List["State"] = Relationship(back_populates="spaces_report", link_model=SpacesReportState)


# Defining the models for the third-party APIs responses
class SpacesReportResponse(BaseModel):
    """Response model for the spaces report created solely for the purpose of documentation"""
    total_spaces_count: int
    total_events_count: int
    average_events_count: float
    most_active_state: str
    most_active_space: str
    events_per_state: Optional[dict[str, int]]

    model_config = {
        "json_schema_extra": {
            "example": {
                "total_spaces_count": 10,
                "total_events_count": 100,
                "average_events_count": 10.0,
                "most_active_state": "CE",
                "most_active_space": "CENTRO DRAGÃO DO MAR DE ARTE E CULTURA",
                "events_per_state": {
                    "CE": 40,
                    "PB": 30,
                    "RN": 20,
                    "PE": 10
                }
            }
        }
    }


class OccurrenceSpaceResponse(BaseModel):
    id: int
    name: str
    En_Estado: str | None

    model_config = {
        "json_schema_extra": {
            "example": {
                "name": "CENTRO DRAGÃO DO MAR DE ARTE E CULTURA",
                "En_estado": "CE"
            }
        }
    }

class OcurrenceResponse(BaseModel):
    """Response model for the event space created solely
    for the purpose of documentation of a third-party API response
    """
    id: int
    space: OccurrenceSpaceResponse | None

    model_config = {
        "json_schema_extra": {
            "example": {
                "id": 1,
                "name": "CENTRO DRAGÃO DO MAR DE ARTE E CULTURA",
                "En_estado": "CE"
            }
        }
    }


class EventsResponse(BaseModel):
    """"""
    id: int
    spaces: list[str]
    occurrences: list[OcurrenceResponse]
    occurrencesReadable: list[str]

    model_config = {
        "json_schema_extra": {
            "example": {
                "id": 1,
                "occurrences": [
                    {
                        "id": 1,
                        "name": "CENTRO DRAGÃO DO MAR DE ARTE E CULTURA",
                        "En_estado": "CE"
                    }
                ]
            }
        }
    }
