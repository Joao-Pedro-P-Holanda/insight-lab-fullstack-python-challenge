"""This script plots the data about spaces on th Cultural Maps from the years 2020 to 2024."""
import os
import matplotlib.pyplot as plt
import httpx
import json
import asyncio
# Fetching Data from the API
async def fetch_data():
    async with httpx.AsyncClient() as client:
        print("Fetching data from the API...")
        responses = await asyncio.gather(
            *[client.get("http://localhost:8000/spaces/report/states", params={
                "start_date": f"{year}-01-01", "end_date": f"{year}-12-31"
                }) for year in range(2020, 2025)]
        )
        print("Data fetched successfully!")
        return responses

responses = asyncio.run(fetch_data())


# Extracting the data from the responses
data = [json.loads(response.json()) for response in responses]


# Creating plots

# Events per state


events_per_state = [d['total_events_count'] for d in data]

for i in range(5):
    events_per_state = data[i]["events_per_state"]
    states = list(events_per_state.keys())
    counts = list(events_per_state.values())
    fig, ax = plt.subplots()
    ax.bar(states, counts)
    ax.set_title(f"Eventos por estado- {2020 + i} \n Total de eventos: {data[i]['total_events_count']}")
    ax.set_xlabel('Estados')
    ax.set_ylabel('Número de eventos realizados')

    file_name = f"events_per_state_{2020+i}.png"
    print(f"Saving plot as image in {file_name}")
    folder_path = os.path.join(os.path.dirname(__file__), "plots")
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    plt.savefig(os.path.join(folder_path, file_name))
