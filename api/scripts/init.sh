#!/bin/sh

cd /code
alembic upgrade head
uvicorn api.main:app --host 0.0.0.0