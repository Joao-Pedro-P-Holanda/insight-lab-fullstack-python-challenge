"""Endpoint responsible for fetching the data of a given space"""
from fastapi import APIRouter, Query
from api.deps import CulturalMapsDep, IBGEEndpointDep, SessionDep, get_cultural_maps_endpoint
from api.queries import DateTimeQuery, StateListQuery
from api.utils.query_parameters_conversion import str_to_state_acronym_list
from api.crud.spaces import SpacesReportCRUD
from api.crud.states import StateCRUD
from fastapi.responses import JSONResponse
from api.models import SpacesReportResponse, EventsResponse

from sqlmodel import select
router = APIRouter()

# router used only to document the callbacks used in the endpoint
cultural_maps_callback_router = APIRouter()

@cultural_maps_callback_router.get(str(get_cultural_maps_endpoint())+"event/findByLocation",response_model=EventsResponse)
def get_events_by_location(at_select:str, at_from:DateTimeQuery, at_to:DateTimeQuery):
    pass

@router.get("/report/states",callbacks=cultural_maps_callback_router.routes, response_model=SpacesReportResponse)
async def get_spaces_report(
                            session: SessionDep,
                            cultural_maps_endpoint: CulturalMapsDep,
                            ibge_endpoint: IBGEEndpointDep,
                            start_date: DateTimeQuery = Query(description="Starting date of the search"),
                            end_date: DateTimeQuery = Query(description="Ending date of the search"),
                            states: StateListQuery = Query(None, description="List of states to be queried, if no value is given fetches from all brazilian states")
                            ):
    """
    This method searches for all events occurring in a given period and returns
    a report with the following information:
    - Total number of spaces
    - Total number of events
    - Average number of events per space
    - The state with the most events
    - The space with the most events
    - Number of events per state
    """

    spacesReportCrud = SpacesReportCRUD(session, cultural_maps_endpoint)
    stateCrud = StateCRUD(session, ibge_endpoint)
    states_list = []
    if states is None:
        all_states = await stateCrud.get_states()
        states_list = [state.acronym for state in all_states]
    else:
        states_list = str_to_state_acronym_list(states)
    stored_report = spacesReportCrud.get_existing_spaces_report(start_date, end_date, states_list)
    if stored_report is not None:
        return JSONResponse(stored_report.model_dump_json())
    report = await spacesReportCrud.get_new_spaces_report(start_date, end_date, states_list)
    return JSONResponse(report.model_dump_json())
