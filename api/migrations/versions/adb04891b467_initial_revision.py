"""initial revision

Revision ID: adb04891b467
Revises: 
Create Date: 2024-06-06 16:38:29.875309

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
import sqlmodel


# revision identifiers, used by Alembic.
revision: str = 'adb04891b467'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('agentreport',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('start_date', sa.DateTime(), nullable=False),
    sa.Column('end_date', sa.DateTime(), nullable=False),
    sa.Column('total_agent_count', sa.Integer(), nullable=False),
    sa.Column('average_events_performed_count', sa.Integer(), nullable=False),
    sa.Column('average_events_attended_count', sa.Integer(), nullable=False),
    sa.Column('most_active_agent', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.Column('most_agents_state', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('spacesreport',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('start_date', sa.DateTime(), nullable=False),
    sa.Column('end_date', sa.DateTime(), nullable=False),
    sa.Column('total_spaces_count', sa.Integer(), nullable=False),
    sa.Column('total_events_count', sa.Integer(), nullable=False),
    sa.Column('average_events_count', sa.Float(), nullable=False),
    sa.Column('most_active_state', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.Column('most_active_space', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('state',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.Column('acronym', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.Column('region', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('state')
    op.drop_table('spacesreport')
    op.drop_table('agentreport')
    # ### end Alembic commands ###
