from typing import Sequence
import httpx
from api.models import State
from api.utils.url import append_paths
from api.deps import SessionDep,IBGEEndpointDep
import logging

from sqlmodel import select

logger = logging.getLogger(__name__)

class StateCRUD:
    def __init__(self, session: SessionDep, ibge_endpoint: IBGEEndpointDep):
        self.ibge_endpoint = ibge_endpoint
        self.session = session

    async def get_states(self)->Sequence[State]:
        statement = select(State)
        states = list(self.session.exec(statement).all())
        if len(states) == 0:
            await self._init_states()
            states = list(self.session.exec(statement).all())
        return states


    async def _init_states(self)->None:
        session = self.session
        try:
            states = []
            for state in await self._get_states_from_api():
                states.append(State(
                    id=state["id"],
                    acronym=state["sigla"],
                    name=state["nome"],
                    region=state["regiao"]["nome"])
                    )
            session.add_all(states)
            session.commit()
        except Exception as e:
            logger.error(f"Error while initializing states on the database: {e}")
            session.rollback()


    async def _get_states_from_api(self):
        url = append_paths(self.ibge_endpoint, "localidades", "estados")
        async with httpx.AsyncClient() as client:
            response = await client.get(url)
            return response.json()
