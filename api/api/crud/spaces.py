from api.deps import SessionDep,CulturalMapsDep
from api.models import SpacesReport, EventsResponse, OcurrenceResponse, State
from collections import namedtuple
from api.utils.url import append_paths
from api.utils.query_parameters_conversion import str_to_datetime
from api.queries import DateTimeQuery
import httpx
from sqlmodel import select
import logging

logger = logging.getLogger(__name__)

SpaceInfo = namedtuple("SpaceInfo", ["name", "state"])

class SpacesReportCRUD:
    def __init__(self, session: SessionDep, cultural_maps_endpoint: CulturalMapsDep):
        self.session = session
        self.cultural_maps_endpoint = cultural_maps_endpoint
        self.events_per_space: dict[str,int] = {}
        self.events_per_state: dict[str,int] = {}
        self.total_events_count = 0
        self.most_active_state = None
        self.most_active_space = None
        self.states_queried: list[str] = []

    async def get_new_spaces_report(self,start_date: DateTimeQuery,
                      end_date: DateTimeQuery,
                      states: list[str],
                    ) -> SpacesReport:
        await self._fetch_events(start_date, end_date,states)
        self.states_queried = states
        self.most_active_space = self._get_most_active_space()
        self.most_active_state = self._get_most_active_state()
        average = self.total_events_count / len(self.events_per_space) if len(self.events_per_space) > 0 else 0

        result = SpacesReport(
            start_date=str_to_datetime(start_date),
            end_date=str_to_datetime(end_date),
            total_spaces_count=len(self.events_per_space),
            total_events_count=self.total_events_count,
            average_events_count= average,
            most_active_state=self.most_active_state,
            most_active_space=self.most_active_space,
            events_per_state=self.events_per_state,
        )
        self.save_to_database(result)
        return result


    async def _fetch_events(self, start_date: DateTimeQuery, end_date: DateTimeQuery, states: list[str])-> None:
        """
        Fetches events that occurred during the start_date - end_date interval from the cultural maps API,
        the information about the spaces is included in the response and is used to determine the metrics return in the report.
        """
        self.states_queried = states

        url = append_paths(self.cultural_maps_endpoint, "event", "findByLocation")
        params = {
            "@select": "id,occurrences.{space.{name,En_Estado}}",
            "@from": start_date,
            "@to": end_date,
        }
        async with httpx.AsyncClient() as client:
            events = (await client.get(url,params=params)).json()
            for event in events:
                # increments the number of events for a given space
                # a single event can have multiple occurrences in different spaces
                # comparing by name to avoid duplicates with different ids
                event = EventsResponse(**event)
                for occurrence in event.occurrences:
                    if occurrence.space is not None and occurrence.space.En_Estado in states:
                        space_name = occurrence.space.name
                        self.events_per_space.update({ space_name: self.events_per_space.get(space_name, 0) + 1})
                        state = occurrence.space.En_Estado
                        self.events_per_state.update({ state: self.events_per_state.get(state, 0) + 1})

            self.total_events_count = sum(self.events_per_state.values())



    def get_existing_spaces_report(self, start_date: DateTimeQuery, end_date: DateTimeQuery, states: list[str]) -> SpacesReport | None:
        """
        Searches for reports that already exist in the database,
        a report can only be reused if it contains the same dates and states as the request
        """
        dates_statement = select(SpacesReport).where(SpacesReport.start_date == str_to_datetime(start_date),
                                               SpacesReport.end_date == str_to_datetime(end_date),
                                               )
        reports_on_same_date = self.session.exec(dates_statement).all()
        # potential bottleneck, querying for reports with 'states' attribute
        # matching the given states is more efficient than querying for all reports and filtering them
        for report in reports_on_same_date:
            if report.states is None:
                pass
            used_states = [state.acronym for state in report.states]
            if set(states) == set(used_states):
                return report
        return None


    def save_to_database(self, report: SpacesReport) -> None:
        """
        Saves the report to the database,
        information about the states that were queried
        is saved in the spacesreportstate tables.
        Note that the list of queried states don't necessarily match the states used in the report,
        since an state could have no events in the given period.
        """
        states_queried_query = select(State).where(State.acronym.in_(self.states_queried))
        states_used = list(self.session.exec(states_queried_query).all())
        report.states = states_used
        try:
            self.session.add(report)
            self.session.commit()
            # after commiting the report, the id is generated and the object needs to be updated
            self.session.refresh(report)
        except Exception as e:
            logger.error(f"Error saving report to database: {e}")
            self.session.rollback()


    def _get_most_active_space(self) -> str:
        """
        Returns the name of the space with the most events
        """
        if len(self.events_per_space) == 0:
            return "N/A"
        return max(self.events_per_space, key=lambda x: self.events_per_space[x])

    def _get_most_active_state(self) -> str:
        """
        Returns the name of the state with the most events
        """
        if len(self.events_per_state) == 0:
            return "N/A"
        return max(self.events_per_state, key=lambda x: self.events_per_state[x])
