"""Script containing all annotated dependencies"""
from typing import Annotated

from fastapi import Depends
from pydantic_core import Url
from sqlmodel import Session
from api.core.database import get_session


def get_cultural_maps_endpoint() -> Url:
    return Url("https://mapas.cultura.gov.br/api/")

def get_ibge_endpoint()->Url:
    return Url("https://servicodados.ibge.gov.br/api/v1/")

SessionDep = Annotated[Session, Depends(get_session)]

IBGEEndpointDep = Annotated[Url, Depends(get_ibge_endpoint)]
CulturalMapsDep = Annotated[Url, Depends(get_cultural_maps_endpoint)]
