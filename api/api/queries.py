from ast import Str
from datetime import datetime
from typing import Annotated
from pydantic import BeforeValidator
from pydantic.functional_validators import AfterValidator


def is_day_month_year(str_date: str):
    """Check if the date is in the format yyyy-mm-dd"""
    try:
        datetime.strptime(str_date, "%Y-%m-%d")
    except ValueError:
        raise ValueError("Date should be in the format dd-mm-yyyy")
    return str_date

DateTimeQuery = Annotated[str, AfterValidator(is_day_month_year)]

def is_list_of_states(states: str | None) -> str|None:
    """Check if the list of states is valid"""
    if states is not None:
        states_as_list = states.split(",")
        for state in states_as_list:
            if not state.isupper():
                raise ValueError("State should be in uppercase")
            if not len(state) == 2:
                raise ValueError("State should have 2 characters")
        return states
    return None

StateListQuery = Annotated[str|None, BeforeValidator(is_list_of_states)]
